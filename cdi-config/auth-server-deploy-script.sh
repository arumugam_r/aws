#!/bin/bash -ex
set -x
#RC_VERSION=$1;
AUTH_RC_VERSION=$1;
export db_super_user="mdm_server";
export PGPASSWORD="mdmServer";
ARTIFACTORY_HOST="192.168.99.122";
db_host="192.168.99.126"
db_port="5432"
db_user="mdm_server";
db_pass="mdmServer";

tomcat_dir="/opt/tomcat-latest/"
deployables_home="/appdata/deployables"
#------------Props------------------------

# For auth
key_auth_db_host="db.host"
key_auth_db_port="db.port"
key_auth_db_name="db.database"
key_auth_db_user="db.user";
key_auth_db_password="db.password";

val_auth_db_name="isha_auth"
val_auth_db_user=$db_user;
val_auth_db_password=$db_pass;
val_auth_db_host=$db_host
val_auth_db_port=$db_port

updateProperty() {
	ss=$1;
	rs=$2;
	fn=$3;

	ss=`echo $ss|sed -e 's/\"/\\\"/g'`
	rs=`echo $rs|sed -e 's/\"/\\\"/g'`

	cat $fn|awk "BEGIN{FS=\"=\";vss=\"$ss\";vrs=\"$rs\"}{if(\$1==vss){print \$1\"=\"vrs}else print \$0;}" > __temp
	mv __temp $fn
}

#------------Create the temporary directories---------
cd /appdata
rm -rf  $deployables_home
mkdir -p $deployables_home && cd $deployables_home

#------------------------------
# Create the tables for each DB
#------------------------------

mkdir db-migration;
cd db-migration;

#export PATH=/appdata/dbsteward/composer/vendor/bin:$PATH
export PATH=$PATH:/home/surendra/.config/composer/vendor/bin/

# get the db-migration script
wget "http://$ARTIFACTORY_HOST:8081/artifactory/cdi-artifacts/static/service/db-migration/db-migration.sh" > /dev/null 2>&1
sed -i "s/__db_host/$db_host/g" db-migration.sh
sed -i "s/__db_port/$db_port/g" db-migration.sh
sed -i "s/__db_user/$db_user/g" db-migration.sh
sed -i "s/__db_password/$db_pass/g" db-migration.sh


export PGPASSWORD="$db_pass";

# perform db-migration for auth db
mkdir isha_auth;
wget "http://$ARTIFACTORY_HOST:8081/artifactory/cdi-artifacts/rc/${AUTH_RC_VERSION}/service/db-migration/auth-db-schema.xml" > /dev/null 2>&1
mv auth-db-schema.xml isha_auth/;
sh db-migration.sh 'isha_auth' 'auth-db-schema.xml';


# moving back to the root of the temp dir
cd $deployables_home;

#-----------Deploy Auth--------------------
echo "auth.war deployment start"
cd $deployables_home
rm -rf temp;
mkdir temp && cd temp;

echo "Downloading the Auth.war."
wget http://$ARTIFACTORY_HOST:8081/artifactory/cdi-artifacts/rc/${AUTH_RC_VERSION}/service/auth.war > /dev/null 2>&1

echo "Unjar the artifacts."
jar -xvf auth.war > /dev/null 2>&1

updateProperty "$key_auth_db_host" "$val_auth_db_host" 'WEB-INF/classes/config/mdm-dao.properties'
updateProperty "$key_auth_db_port" "$val_auth_db_port" 'WEB-INF/classes/config/mdm-dao.properties'
updateProperty "$key_auth_db_name" "$val_auth_db_name" 'WEB-INF/classes/config/mdm-dao.properties'
updateProperty "$key_auth_db_user" "$val_auth_db_user" 'WEB-INF/classes/config/mdm-dao.properties'
updateProperty "$key_auth_db_password" "$val_auth_db_password" 'WEB-INF/classes/config/mdm-dao.properties'

rm auth.war;
jar -cvf auth.war *  > /dev/null 2>&1
rm -rf $tomcat_dir/webapps/auth*;
mv auth.war $tomcat_dir/webapps/
echo "auth.war deployed."

#-------------Create other folders---------------
# auth-keys
mkdir -p /appdata/auth/keys
wget "http://$ARTIFACTORY_HOST:8081/artifactory/cdi-artifacts/static/service/auth-keys/key_rsa.pvt" > /dev/null 2>&1
mv key_rsa.pvt /appdata/auth/keys/
chmod -R g+rx /appdata/auth
chgrp -R tomcat /appdata/auth

echo "Starting the Tomcat server"
# the below cd is required as a workaround for the velocity-initialization issue
cd $tomcat_dir/webapps;
/etc/init.d/tomcat0 restart
cd -


exit 0

