
db_host=$1
db_port=$2
db_database=$3
db_user=$4
db_pass=$5

export PGPASSWORD=$db_pass

psql -U $db_user -h $db_host -p $db_port -w $db_database -v ON_ERROR_STOP=1 \
-c "update system set communication_info = '{\"userId\": \"deployer\", \"authApi\": {\"baseUrl\": \"http://192.168.99.126:8090/auth/api\", \"endPoint\":
\"/token/issue\", \"inputType\": \"\", \"httpMethod\": \"POST\", \"outputType\": \"\", \"isArrayInputType\": false, \"isArrayOutputType\": false}, \"baseUrl\": \"\", \"clientId\": \"\", \"password\": \"\", \"isIshaAuth\": true, \"authBaseUrl\": \"\", \"clientSecret\": \"\", \"securityToken\": \"eyJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1MjUyNDg2MjEsImlzcyI6ImRldi1hcGkuaXNoYS5pbiIsIl9zaSI6NiwiX2F0diI6NzIwMDAwMDAsIl90dCI6MX0.R8mkEYHX1EOR-JCibPLMye81iIsudT3c6UQqC3nU1AbRC43BwKnwDIdCt5L3xrHw0yKoDhpQHy7jwzXeOECmnAChqlzNqFJjtwolu10fulYcqQFgHQei2Kr7m0KVAj0j_vShXzpVWfjejaiKKR_HHvFUqIw-XvraL3E6IDfsGHs\", \"loginAccessToken\": \"\"}' where system_id = 6;"

psql -U $db_user -h $db_host -p $db_port -w $db_database -v ON_ERROR_STOP=1 \
-c "update job_system_map set api_info = '{\"baseUrl\": \"http://ishademo.serveblog.net:5050\", \"endPoint\": \"/al-ib/apisForCdi.php?mode=getContactsByTime\", \"inputType\": \"fetchByDateVO.class\", \"httpMethod\": \"POST\", \"outputType\": \"fetchContactByTimestampVO.class\", \"isArrayInputType\": false, \"isArrayOutputType\": false}' where row_key = '6|1';"
