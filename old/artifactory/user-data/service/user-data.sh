#!/bin/bash -ex

echo get the instance-id from meta-data.
IP=`wget http://169.254.169.254/latest/meta-data/local-ipv4  -qO - | cut -d . -f 1,2,3,4`

echo name variable for the hostname $IP
HOSTNAME=service0
MONITOR=10.0.32.153

echo update the hostname file with the new hostname/fqdn  $HOSTNAME.ishacloud.org
echo $HOSTNAME.ishacloud.org > /etc/hostname

echo update the running hostname
hostname $HOSTNAME.ishacloud.org

echo Crate hosts file with Hostname and IP
echo "127.0.0.1 localhost" > /etc/hosts
echo $IP  $HOSTNAME.ishacloud.org >> /etc/hosts


echo Add monitor hosts ip in the /etc/hosts file
echo $MONITOR  monitor.ishacloud.org >> /etc/hosts

#---------------------------------- Nagios NRPE Plugin Configuration Start -------------------------------

sed -i '/allowed_hosts/c\allowed_hosts=127.0.0.1,monitor.ishacloud.org' /etc/nagios/nrpe.cfg

#---------------------------------- Nagios NRPE Plugin Configuration End -------------------------------



export db_super_user="ishasrvprod";
export PGPASSWORD="isha_srv_it_230";

db_host="isha-srv-prod.c9natp3syneo.us-east-1.rds.amazonaws.com"
db_port="5432"
db_user="mdm_server";
db_pass="mdmServer";

solr_host="10.0.36.128";
solr_port="8983";

redis_host="10.0.36.128";
redis_port="6379";

LOCAL_HOST="$IP"

tomcat_dir="/appdata/apache-tomcat-8.0.21"
deployables_home="/appdata/deployables"
#------------Props------------------------

# For auth
k_auth_db_host="db.host"
k_auth_db_port="db.port"
k_auth_db_name="db.database"
k_auth_db_user="db.user";
k_auth_db_password="db.password";

v_auth_db_name="isha_auth"
v_auth_db_user=$db_user;
v_auth_db_password=$db_pass;
v_auth_db_host=$db_host
v_auth_db_port=$db_port

# For CDI
k_cdi_db_host="db.host"
k_cdi_db_port="db.port"
k_cdi_db_name="db.database"
k_cdi_db_user="db.user";
k_cdi_db_password="db.password";
k_cdi_solr_host="solr.host"
k_cdi_solr_port="solr.port"
k_cdi_redis_host="redis.host"
k_cdi_redis_port="redis.port"

v_cdi_db_name="cdi_server_db"
v_cdi_db_user=$db_user;
v_cdi_db_password=$db_pass;
v_cdi_db_host=$db_host
v_cdi_db_port=$db_port
v_cdi_solr_host="$solr_host:2181/solr"
v_cdi_solr_port=$solr_port
v_cdi_redis_host=$redis_host
v_cdi_redis_port=$redis_port

# For ib-independent

k_ib_independent_db_host="db.host"
k_ib_independent_db_port="db.port"
k_ib_independent_db_name="db.database"
k_ib_independent_db_user="db.user";
k_ib_independent_db_password="db.password";
k_ib_independent_redis_host="redis.host"
k_ib_independent_redis_port="redis.port"
k_ib_independent_ibcdibaseurl="ibCdiBaseUrl"
k_ib_independent_cdibaseurl="cdiBaseUrl"
k_ib_independent_authurl="ishaAuth.baseUrl"
k_ib_independent_txnStoreBaseUrl="txnStoreBaseUrl"

v_ib_independent_db_name="ib_database"
v_ib_independent_db_user=$db_user;
v_ib_independent_db_password=$db_pass;
v_ib_independent_db_host=$db_host
v_ib_independent_db_port=$db_port
v_ib_independent_redis_host=$redis_host
v_ib_independent_redis_port=$redis_port
v_ib_independent_ibcdibaseurl="http://$LOCAL_HOST:8080/ib-cdi/api/"
v_ib_independent_cdibaseurl="http://$LOCAL_HOST:8080/IshaMDMService/api/"
v_ib_independent_authurl="http://$LOCAL_HOST:8080/auth/api"
v_ib_independent_txnStoreBaseUrl="http://$LOCAL_HOST:8080/bi_service/api"



# for BI_service
k_bi_client_address="cxf.jaxrs.client.address"
k_bi_datasource_url="spring.datasource.url"
k_bi_db_host="db.host"
k_bi_db_port="db.port"
k_bi_db_name="db.database"
k_bi_db_user="spring.datasource.username";
k_bi_db_password="spring.datasource.password";

v_bi_db_host="$db_host"
v_bi_db_port="$db_port"
v_bi_db_name="bi_db"
v_bi_db_user="$db_user";
v_bi_db_password="$db_pass";
v_bi_client_address="http://${LOCAL_HOST}:8080/bi_service/services"
v_bi_datasource_url="jdbc:postgresql://${db_host}:${db_port}/${v_bi_db_name}"



cdb() {

	var_db_name=$1;
	var_db_owner=$2;

	db=`echo "select count(*) from pg_catalog.pg_database where datname='$var_db_name'" | psql -U $db_super_user -h $db_host postgres -t`
	echo "#$db found with name : $var_db_name"
    db=$(($db + 0))
	#db=`expr $db + 0`
	if [ $db -ne 0 ]
	then
		echo "No need to create the DB"
	else
		echo "DB needs to be created"
		echo "create database $var_db_name owner $db_super_user;"| psql -U $db_super_user  -h $db_host postgres -t
		echo "alter database $var_db_name owner to $var_db_owner;"| psql -U $db_super_user  -h $db_host postgres -t
	fi

}

crole() {

	var_db_user=$1;
	var_db_user_pass=$2;

	role=`echo "SELECT count(*) FROM pg_roles WHERE rolname ='$db_user'"| psql -U $db_super_user -h $db_host postgres -t`;
	echo "#$role found with name : $var_db_user"
    role=$(($role + 0))
	#role=`expr $role + 0`
	if [ $role -ne 0 ]
	then
		echo "No need to create the role"
	else
		echo "Role needs to be created"
		echo "create role $var_db_user with password '$var_db_user_pass' login;"| psql -U $db_super_user -h $db_host postgres -t
	fi

}

up() {
	ss=$1;
	rs=$2;
	fn=$3;

	ss=`echo $ss|sed -e 's/\"/\\\"/g'`
	rs=`echo $rs|sed -e 's/\"/\\\"/g'`

	cat $fn|awk "BEGIN{FS=\"=\";vss=\"$ss\";vrs=\"$rs\"}{if(\$1==vss){print \$1\"=\"vrs}else print \$0;}" > __temp
	mv __temp $fn
}

#--------------Create DBs and User---------------------
crole $db_user $db_pass
cdb "ib_database" $db_user
cdb "cdi_server_db" $db_user
cdb "isha_auth" $db_user
cdb "bi_db" $db_user


#------------Create the temporary directories---------
cd /appdata
rm -rf  $deployables_home
mkdir -p $deployables_home && cd $deployables_home

#------------------------------
# Create the tables for each DB
#------------------------------

mkdir db-migration;
cd db-migration;

export PATH=/appdata/dbsteward/composer/vendor/bin:$PATH

# get the db-migration script
wget 'https://s3.amazonaws.com/isha-bucket/prod/artifactory/latest/service/db-migration/db-migration.sh' > /dev/null 2>&1
sed -i "s/__db_host/$db_host/g" db-migration.sh
sed -i "s/__db_port/$db_port/g" db-migration.sh
sed -i "s/__db_user/$db_user/g" db-migration.sh
sed -i "s/__db_password/$db_pass/g" db-migration.sh


export PGPASSWORD="$db_pass";

# perform db-migration for auth db
mkdir isha_auth;
wget 'https://s3.amazonaws.com/isha-bucket/prod/artifactory/latest/service/db-migration/auth-db-schema.xml' > /dev/null 2>&1
mv auth-db-schema.xml isha_auth/;
sh db-migration.sh 'isha_auth' 'auth-db-schema.xml';


# perform db-migration for ib database
mkdir ib_database;
wget 'https://s3.amazonaws.com/isha-bucket/prod/artifactory/latest/service/db-migration/ib-db-schema.xml' > /dev/null 2>&1
mv ib-db-schema.xml ib_database/;
sh db-migration.sh 'ib_database' 'ib-db-schema.xml';

# perform db-migration for cdi database
mkdir cdi_server_db;
wget 'https://s3.amazonaws.com/isha-bucket/prod/artifactory/latest/service/db-migration/cdi-db-schema.xml' > /dev/null 2>&1
mv cdi-db-schema.xml cdi_server_db/;
sh db-migration.sh 'cdi_server_db' 'cdi-db-schema.xml';

# perform db-migration for bi
mkdir bi_db;
wget 'https://s3.amazonaws.com/isha-bucket/prod/artifactory/latest/service/db-migration/bi-db-schema.xml' > /dev/null 2>&1
mv bi-db-schema.xml bi_db/;
sh db-migration.sh 'bi_db' 'bi-db-schema.xml';


# moving back to the root of the temp dir
cd $deployables_home;


#------------Deploy the WARs--------------------
echo "MDM Deployment start."
echo "Stopping tomcat"
service tomcat stop;

#delete the existing wars;
rm $tomcat_dir/webapps/*.war;

cd $deployables_home;
rm -rf temp;
mkdir temp;
cd temp;
wget https://s3.amazonaws.com/isha-bucket/prod/artifactory/latest/service/IshaMDMService.war > /dev/null 2>&1

echo "Unjar the artifacts."
jar -xvf IshaMDMService.war > /dev/null 2>&1

up "$k_cdi_db_host" "$v_cdi_db_host" 'WEB-INF/classes/config/mdm-dao.properties'
up "$k_cdi_db_port" "$v_cdi_db_port" 'WEB-INF/classes/config/mdm-dao.properties'
up "$k_cdi_db_name" "$v_cdi_db_name" 'WEB-INF/classes/config/mdm-dao.properties'
up "$k_cdi_db_user" "$v_cdi_db_user" 'WEB-INF/classes/config/mdm-dao.properties'
up "$k_cdi_db_password" "$v_cdi_db_password" 'WEB-INF/classes/config/mdm-dao.properties'
up "$k_cdi_solr_host" "$v_cdi_solr_host" 'WEB-INF/classes/config/mdm-dao.properties'
up "$k_cdi_solr_port" "$v_cdi_solr_port" 'WEB-INF/classes/config/mdm-dao.properties'
up "$k_cdi_redis_host" "$v_cdi_redis_host" 'WEB-INF/classes/config/mdm-dao.properties'
up "$k_cdi_redis_port" "$v_cdi_redis_port" 'WEB-INF/classes/config/mdm-dao.properties'

rm IshaMDMService.war;
jar -cvf IshaMDMService.war * > /dev/null 2>&1
rm -rf $tomcat_dir/webapps/IshaMDMService*;
mv IshaMDMService.war $tomcat_dir/webapps/

#-----------Deploy Auth--------------------
cd $deployables_home
rm -rf temp;
mkdir temp && cd temp;

wget https://s3.amazonaws.com/isha-bucket/prod/artifactory/latest/service/auth.war > /dev/null 2>&1
jar -xvf auth.war > /dev/null 2>&1

up "$k_auth_db_host" "$v_auth_db_host" 'WEB-INF/classes/config/mdm-dao.properties'
up "$k_auth_db_port" "$v_auth_db_port" 'WEB-INF/classes/config/mdm-dao.properties'
up "$k_auth_db_name" "$v_auth_db_name" 'WEB-INF/classes/config/mdm-dao.properties'
up "$k_auth_db_user" "$v_auth_db_user" 'WEB-INF/classes/config/mdm-dao.properties'
up "$k_auth_db_password" "$v_auth_db_password" 'WEB-INF/classes/config/mdm-dao.properties'

rm auth.war;
jar -cvf auth.war *  > /dev/null 2>&1
rm -rf $tomcat_dir/webapps/auth*;
mv auth.war $tomcat_dir/webapps/

#---------------Deploy ib-----------------------
cd $deployables_home
rm -rf temp;
mkdir temp && cd temp;

wget https://s3.amazonaws.com/isha-bucket/prod/artifactory/latest/service/ib.war > /dev/null 2>&1
jar -xvf ib.war > /dev/null 2>&1

#-----------------
# ./WEB-INF/classes/framework/config/service.properties
up "$k_ib_independent_db_host" "$v_ib_independent_db_host" 'WEB-INF/classes/framework/config/service.properties'
up "$k_ib_independent_db_port" "$v_ib_independent_db_port" 'WEB-INF/classes/framework/config/service.properties'
up "$k_ib_independent_db_name" "$v_ib_independent_db_name" 'WEB-INF/classes/framework/config/service.properties'
up "$k_ib_independent_db_user" "$v_ib_independent_db_user" 'WEB-INF/classes/framework/config/service.properties'
up "$k_ib_independent_db_password" "$v_ib_independent_db_password" 'WEB-INF/classes/framework/config/service.properties'
up "$k_ib_independent_redis_host" "$v_ib_independent_redis_host" 'WEB-INF/classes/framework/config/service.properties'
up "$k_ib_independent_redis_port" "$v_ib_independent_redis_port" 'WEB-INF/classes/framework/config/service.properties'
up "$k_ib_independent_ibcdibaseurl" "$v_ib_independent_ibcdibaseurl" 'WEB-INF/classes/framework/config/service.properties'
up "$k_ib_independent_cdibaseurl" "$v_ib_independent_cdibaseurl" 'WEB-INF/classes/framework/config/service.properties'
up "$k_ib_independent_authurl" "$v_ib_independent_authurl" 'WEB-INF/classes/framework/config/service.properties'
up "$k_ib_independent_txnStoreBaseUrl" "$v_ib_independent_txnStoreBaseUrl" 'WEB-INF/classes/framework/config/service.properties'

rm ib.war;
jar -cvf ib.war *  > /dev/null 2>&1
rm -rf $tomcat_dir/webapps/ib.war;
mv ib.war $tomcat_dir/webapps/

chown -R tomcat:root $tomcat_dir/webapps/ && chmod -R 755 $tomcat_dir/webapps


#---------------Deploy bi-----------------------
cd $deployables_home
rm -rf temp;
mkdir temp && cd temp;

wget https://s3.amazonaws.com/isha-bucket/prod/artifactory/latest/service/bi_service.war > /dev/null 2>&1
jar -xvf bi_service.war > /dev/null 2>&1

#-----------------
# ./WEB-INF/classes/application.properties

up "$k_bi_client_address" "$v_bi_client_address" 'WEB-INF/classes/application.properties'
up "$k_bi_datasource_url" "$v_bi_datasource_url" 'WEB-INF/classes/application.properties'
up "$k_bi_db_host" "$v_bi_db_host" 'WEB-INF/classes/application.properties'
up "$k_bi_db_port" "$v_bi_db_port" 'WEB-INF/classes/application.properties'
up "$k_bi_db_name" "$v_bi_db_name" 'WEB-INF/classes/application.properties'
up "$k_bi_db_user" "$v_bi_db_user" 'WEB-INF/classes/application.properties'
up "$k_bi_db_password" "$v_bi_db_password" 'WEB-INF/classes/application.properties'

rm bi_service.war;
jar -cvf bi_service.war *  > /dev/null 2>&1
rm -rf $tomcat_dir/webapps/bi_service.war;
mv bi_service.war $tomcat_dir/webapps/

chown -R tomcat:root $tomcat_dir/webapps/ && chmod -R 755 $tomcat_dir/webapps

#-------------Setting Tomcat ENV----------------
cd $deployables_home;
mkdir tomcat;
cd tomcat;
wget 'https://s3.amazonaws.com/isha-bucket/prod/artifactory/latest/service/tomcat/setenv.sh' > /dev/null 2>&1
cp setenv.sh $tomcat_dir/bin/
cd $deployables_home;


#-------------Create other folders---------------
# auth-keys
mkdir -p /appdata/auth/keys
wget 'https://s3.amazonaws.com/isha-bucket/prod/artifactory/latest/service/auth-keys/key_rsa.pvt' > /dev/null 2>&1
mv key_rsa.pvt /appdata/auth/keys/
chmod -R g+rx /appdata/auth
chgrp -R tomcat /appdata/auth

# batch-uploads
mkdir -p /appdata/mdm/batch-uploads/
chmod -R g+rwx /appdata/mdm
chgrp -R tomcat /appdata/mdm

echo "Starting the Tomcat server"
service tomcat restart

#------------------------------------
# set the cron jobs
#------------------------------------
# cd $deployables_home;
# mkdir -p /appdata/crons;
# mkdir -p /appdata/crons/logs;
# cd /appdata/crons;

# download the crons
# wget 'https://s3.amazonaws.com/isha-bucket/prod/artifactory/latest/service/crons/all-crons.sh' > /dev/null 2>&1

# install the crons
# (crontab -l 2>/dev/null; echo "0 18 * * * /appdata/crons/all-crons.sh batch-match >> /appdata/crons/logs/bm.log 2>&1")| crontab -
# (crontab -l 2>/dev/null; echo "0 18 * * * /appdata/crons/all-crons.sh ib-sync >> /appdata/crons/logs/ib_sync.log 2>&1")| crontab -


#--------------NGINX setup----------------#
cd $deployables_home;
echo "Downloading & Setting up the certificates ca.crt, ishadontin.key and ishadotin.pem"

wget "https://s3.amazonaws.com/isha-bucket/prod/artifactory/latest/api/ca.crt"  > /dev/null 2>&1
mkdir -p /etc/ssl/Isha/certs
mv ca.crt /etc/ssl/Isha/certs/ca.crt
chmod 644 /etc/ssl/Isha/certs/ca.crt

wget "https://s3.amazonaws.com/isha-bucket/prod/artifactory/latest/api/ishadotin.key"  > /dev/null 2>&1
mkdir -p /etc/nginx/Isha/cert
mv ishadotin.key /etc/nginx/Isha/cert/ishadotin.key
chmod 644 /etc/nginx/Isha/cert/ishadotin.key

wget "https://s3.amazonaws.com/isha-bucket/prod/artifactory/latest/api/ishadotin.pem"  > /dev/null 2>&1
mv ishadotin.pem /etc/nginx/Isha/cert/ishadotin.pem
chmod 644 /etc/nginx/Isha/cert/ishadotin.pem

echo "Downloading & Setting up  the NGINX Config ib.conf."
wget "https://s3.amazonaws.com/isha-bucket/prod/artifactory/latest/api/ib-proto.conf"  > /dev/null 2>&1
rm -f /etc/nginx/conf.d/default.conf
mv ib-proto.conf ib.conf
mv ib.conf /etc/nginx/conf.d/ib.conf
chmod 644  /etc/nginx/conf.d/ib.conf

wget "https://s3.amazonaws.com/isha-bucket/prod/artifactory/latest/api/cdi-ui.tar.gz"  > /dev/null 2>&1

# Clear the previous files
rm -rf /appdata/ui
#Unwrap and deploy it to nginx directories.
mkdir -p /appdata/ui
tar -xvf cdi-ui.tar.gz -C  /appdata/ui
#Directories permission changes.
chown -R nginx:root /appdata/ui && chmod -R 755 /appdata/ui

# originally IP address filled by terraform was used as the replacement string
# Now hardcoding as localhost
sed -i 's/<service_url>/localhost/g' /etc/nginx/conf.d/ib.conf

#---------------------------------- NGINX End-------------------------------

echo "Restarting NGINX server.";
service nginx restart

exit 0

	