#! /bin/bash -ex

db_host="__db_host";
db_port="__db_port"
db_user="__db_user";
db_password="__db_password";

database=$1
new_schema_file=$2;

cd $database;
mv $new_schema_file new_schema.xml

dbsteward --sqlformat=pgsql8 --dbschemadump \
--dbhost=$db_host --dbport=$db_port --dbuser=$db_user --dbpassword=$db_password \
--dbname=$database --outputfile=old_schema.xml

dbsteward --oldxml=old_schema.xml --newxml=new_schema.xml

psql -U $db_user -h $db_host -p $db_port -w $database -v ON_ERROR_STOP=1 \
-f new_schema_upgrade_stage1_schema1.sql

psql -U $db_user -h $db_host -p $db_port -w $database -v ON_ERROR_STOP=1 \
-f new_schema_upgrade_stage2_data1.sql

psql -U $db_user -h $db_host -p $db_port -w $database -v ON_ERROR_STOP=1 \
-f new_schema_upgrade_stage3_schema1.sql

psql -U $db_user -h $db_host -p $db_port -w $database -v ON_ERROR_STOP=1 \
-f new_schema_upgrade_stage4_data1.sql

