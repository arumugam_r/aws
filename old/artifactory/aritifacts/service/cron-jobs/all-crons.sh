#!/bin/bash -e

HOST_NAME="__host_name__";
TOKEN="";

function getToken(){
	local var_inp=$1;
	local token_val=`echo "$var_inp"|python -c 'import json,sys;obj=json.load(sys.stdin);print obj["token"]' 2>&1`
	local res=`echo $token_val|grep -iPo "error"`;
	if [ ${#res} -ne 0 ]; then
		exit 1;
	else
		echo "$token_val";
	fi
}


function batch_match(){
	## calling the Batch-API
	res=`curl -s -X POST -H "X-Application-ID: 2" -H "Authorization: Bearer $TOKEN==" -H "Content-Type: application/json" -H "From: user001" -H "x-forwarded-host : ${HOST_NAME}" -H "x-forwarded-server : ${HOST_NAME}"  "https://localhost/auth/v1/token/issue"`
	echo "$res";
}

function ib_sync(){
	## calling the sync-API
	res=`curl -s -X POST -H "X-Application-ID: 2" -H "Authorization: Bearer $TOKEN==" -H "Content-Type: application/json" -H "From: user001" -H "x-forwarded-host : ${HOST_NAME}" -H "x-forwarded-server : ${HOST_NAME}"  "https://localhost/auth/v1/token/issue"`
	echo "$res";
}

function initToken(){
	TOKEN=`curl -s -X POST -H "X-Application-ID: 2" -H "Authorization: Basic dXNlcjAwMTozMjI1MDE3MGEwZGNhOTJkNTNlYzk2MjRmMzM2Y2EyNA==" -H "Content-Type: application/json" -H "From: user001" -H "x-forwarded-host : ${HOST_NAME}" -H "x-forwarded-server : ${HOST_NAME}"  "https://localhost/auth/v1/token/issue"`
	# TOKEN='{"token":"aaaa"}'

	res=$(getToken $TOKEN);
	echo "Got the token. Proceeding with the next step";
}

dt=`date`
echo "=========================================================="
echo "Date : $dt"

case "$1" in
batch-match)
	initToken
	batch_match
	;;
ib-sync)
	initToken
	ib_sync
	;;
*)
echo "Usage: $0 {batch-match|ib-sync}"
esac

echo "Done"
echo "==========================================================="
