#!/bin/bash
HOST="localhost"
PORT="8983"
dt=`date`

echo ">> Batch-Match Initiating : $dt"
echo "======================================"

curl -X POST -H "Content-type: application/json" "from: developer.mdm@ishafoundation.org" -H "x-request-id: 98765" -H "x-application-id: 1" "http://${HOST}:${PORT}/IshaMDMService/api/devUtil/startBatchMatch"

echo "======================================"
