export CATALINA_OPTS="-Xms512m  -Xmx1024m  -XX:MetaspaceSize=256m -XX:MaxMetaspaceSize=512m -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/appdata/apache-tomcat-8.0.21/temp/ -verbose:gc -XX:+UseG1GC -Xloggc:/appdata/apache-tomcat-8.0.21/logs/gc.log -XX:+PrintGCDateStamps -XX:+PrintGCDetails -XX:+UseGCLogFileRotation -XX:GCLogFileSize=100M -XX:NumberOfGCLogFiles=10"

IP=`/sbin/ifconfig eth0 | awk '/inet addr/{print substr($2,6)}'`
export JAVA_OPTS="-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=9999 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djava.rmi.server.hostname=$IP"





