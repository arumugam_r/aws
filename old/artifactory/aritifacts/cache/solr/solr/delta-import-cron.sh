#!/bin/bash
HOST="localhost"
PORT="8983"
dt=`date`

echo "#####################################"
echo "##Delta Import Initiating : $dt"
echo "#####################################"

curl "http://${HOST}:${PORT}/solr/cdi-search/dataimport?command=delta-import&indent=true&wt=json"

echo "############# Done : $dt ##############"

