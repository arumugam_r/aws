#--------------------------------------------------------------
# Service Layer VPC Setup
#--------------------------------------------------------------


name              = "isha-srv"
env               = "stage"
sub_domain        = "isha.in"
region            = "ap-southeast-1"
azs               = "ap-southeast-1a,ap-southeast-1b" # AZs are region specific

#--------------------------------------------------------------
# Network
#--------------------------------------------------------------

service_vpc_cidr            = "30.0.32.0/20"

public_subnets              = "30.0.32.0/24"              # Creating one public subnet only on one AZ for NAT
ib_private_subnets          = "30.0.34.0/24,30.0.35.0/24" # Creating one private subnet per AZ
cdi_private_subnets         = "30.0.36.0/24,30.0.37.0/24" # Creating one private subnet per AZ
solr_private_subnets        = "30.0.38.0/24,30.0.39.0/24" # Creating one private subnet per AZ


key_name = "TestCenter-V2"


#--------------------------------------------------------------
# EC2 Instances
#--------------------------------------------------------------
ib_instance_type          = "t2.nano"
ib_ami                    = "ami-21d30f42"

cdi_instance_type         = "t2.nano"
cdi_ami                   = "ami-21d30f42"


#--------------------------------------------------------------
# NAT Instance instead of NAT gateway.
#--------------------------------------------------------------

nat_instance_type         = "t2.nano"
nat_ami                   = "ami-21d30f42"

