variable "azs"                { }
variable "name"               { }
variable "region"             { }
variable "sub_domain"         { }
variable "key_name"           { }
variable "env"                { }
variable "service_vpc_cidr"     { }
variable "ib_private_subnets"   { }
variable "cdi_private_subnets"  { }
variable "solr_private_subnets" { }
variable "public_subnets"       { }
variable "ib_ami"               { }
variable "ib_instance_type"     { }
variable "cdi_ami"              { }
variable "cdi_instance_type"    { }
variable "nat_ami"              { }
variable "nat_instance_type"    { }

provider "aws" {
  region = "${var.region}"
  access_key = "AKIAIBVWJ6R7IYGOWIMQ"
  secret_key = "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"
}

module "network" {
  source = "../../modules/network"

  name                  = "${var.name}-${var.env}"
  azs                   = "${var.azs}"
  region                = "${var.region}"
  key_name              = "${var.key_name}"
  vpc_cidr              = "${var.service_vpc_cidr}"
  public_subnets        = "${var.public_subnets}"
  ib_private_subnets    = "${var.ib_private_subnets}"
  cdi_private_subnets   = "${var.cdi_private_subnets}"
  solr_private_subnets  = "${var.solr_private_subnets}"
  nat_ami               = "${var.nat_ami}"
  nat_instance_type     = "${var.nat_instance_type}"
}

module "compute" {
  source = "../../modules/compute"

  name                = "${var.name}-${var.env}"
  region              = "${var.region}"
  azs                 = "${var.azs}"
  env                 = "${var.env}"
  key_name            = "${var.key_name}"
  vpc_id              = "${module.network.vpc_id}"
  vpc_cidr            = "${module.network.vpc_cidr}"
  ib_ami              = "${var.ib_ami}"
  ib_instance_type    = "${var.ib_instance_type}"
  ib_subnet_ids       = "${module.network.ib_subnet_ids}"
  cdi_ami             = "${var.cdi_ami}"
  cdi_instance_type   = "${var.cdi_instance_type}"
  cdi_subnet_ids      = "${module.network.cdi_subnet_ids}"
}

