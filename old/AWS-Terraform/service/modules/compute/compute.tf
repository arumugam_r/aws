#--------------------------------------------------------------
# This module creates all compute resources
#--------------------------------------------------------------

variable "name"               { }
variable "region"             { }
variable "vpc_id"             { }
variable "vpc_cidr"           { }
variable "env"                { }
variable "key_name"           { }
variable "ib_subnet_ids"      { }
variable "cdi_subnet_ids"     { }
variable "azs"                { }
variable "ib_ami"             { }
variable "ib_instance_type"   { }
variable "cdi_ami"            { }
variable "cdi_instance_type"  { }

module "ib-autoscale" {
  source = "./autoscaler"
  name                = "${var.name}-ib"
  vpc_id              = "${var.vpc_id}"
  vpc_cidr            = "${var.vpc_cidr}"
  key_name            = "${var.key_name}"
  subnet_ids          = "${var.ib_subnet_ids}"
  ami                 = "${var.ib_ami}"
  azs                 = "${var.azs}"
  env                 = "${var.env}"
  instance_type       = "${var.ib_instance_type}"
}

module "cdi-autoscale" {
  source = "./autoscaler"
  name                = "${var.name}-cdi"
  vpc_id              = "${var.vpc_id}"
  vpc_cidr            = "${var.vpc_cidr}"
  key_name            = "${var.key_name}"
  subnet_ids          = "${var.cdi_subnet_ids}"
  ami                 = "${var.cdi_ami}"
  azs                 = "${var.azs}"
  env                 = "${var.env}"
  instance_type       = "${var.cdi_instance_type}"
}

#output "ib_private_ips"  { value = "${module.ib.private_ips}" }
#output "ib_public_ips"   { value = "${module.ib.public_ips}" }
