#--------------------------------------------------------------
# Auctoscale instance scale policy
#--------------------------------------------------------------

variable "version" {
  default = "v1"
}


variable "desired" {
    default = 1
}

variable "minimum" {
    default = 1
}

variable "maximum" {
    default = 3
}

variable "cooldown" {
    default = 30
}

variable "timeout" {
    default = "5m"
}
