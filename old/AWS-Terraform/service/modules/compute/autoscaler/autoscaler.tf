#--------------------------------------------------------------
# This module creates all resources necessary for HAProxy
#--------------------------------------------------------------

variable "name"              { default = "auto" }
variable "vpc_id"            { }
variable "vpc_cidr"          { }
variable "key_name"          { }
variable "subnet_ids"        { }
variable "ami"               { }
variable "instance_type"     { }
variable "azs"               { }
variable "env"               { }

resource "aws_security_group" "security_group" {
  name        = "${var.name}-autoscale-security_group"
  vpc_id      = "${var.vpc_id}"
  description = "${var.name} Security Group"

  tags      {
    Name = "${var.name}-autoscale-security_group"
  }
  lifecycle { create_before_destroy = true }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
data "template_file" "script" {
  template = "${file("${path.module}/script.tpl")}"

  vars {
    node_name         = "${var.name}"
  }
}

# Render a multi-part cloudinit config making use of the part
# above, and other source files
data "template_cloudinit_config" "config" {
  gzip          = true
  base64_encode = true

  # Setup hello world script to be called by the cloud-config
  part {
    filename     = "init.cfg"
    content_type = "text/part-handler"
    content      = "${data.template_file.script.rendered}"
  }

}

resource "aws_launch_configuration" "launch_conf" {
  name = "${var.name}-launch_config_${var.version}"
  image_id = "${var.ami}"
  instance_type = "${var.instance_type}"
  key_name      = "${var.key_name}"
  security_groups = ["${aws_security_group.security_group.id}"]
  associate_public_ip_address = false
  user_data     = "${data.template_cloudinit_config.config.rendered}"
  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_autoscaling_group" "auto" {
  name = "${var.name}-autoscale_${var.version}"
  launch_configuration = "${aws_launch_configuration.launch_conf.name}"
  desired_capacity = "${var.desired}"
  #availability_zones = ["${var.azs}"]
  vpc_zone_identifier = ["${var.subnet_ids}"]
  min_size = "${var.minimum}"
  max_size = "${var.maximum}"
  default_cooldown = "${var.cooldown}"
  wait_for_capacity_timeout = "${var.timeout}"
  lifecycle {
    create_before_destroy = true
  }
  tag {
    key = "Name"
    value = "${var.name}-auto"
    propagate_at_launch = true
  }
  tag {
    key = "Env"
    value = "${var.env}"
    propagate_at_launch = true
  }
  tag {
    key = "Group"
    value = "service"
    propagate_at_launch = true
  }
}
