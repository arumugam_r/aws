#--------------------------------------------------------------
# This module creates all resources necessary for EC2 based Nat instance
#--------------------------------------------------------------

variable "name"              { default = "nat" }
variable "vpc_id"            { }
variable "vpc_cidr"          { }
variable "key_name"          { }
variable "subnet_ids"        { }
variable "ami"               { }
variable "instance_type"     { }

resource "aws_security_group" "nat" {
  name        = "${var.vpc_id}-${var.name}"
  vpc_id      = "${var.vpc_id}"
  description = "${var.name} EC2 based NAT service"

  tags      {
    Name        = "${var.vpc_id}-${var.name}"
  }
  lifecycle { create_before_destroy = true }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "nat" {
  ami           = "${var.ami}"
  instance_type = "${var.instance_type}"
  key_name      = "${var.key_name}"
  subnet_id     = "${element(split(",", var.subnet_ids), count.index)}"

  vpc_security_group_ids = ["${aws_security_group.nat.id}"]
  associate_public_ip_address = true
  tags      { Name = "${var.name}" }
  lifecycle { create_before_destroy = true }
}

output "nat_id" { value = "${aws_instance.nat.id}"}
