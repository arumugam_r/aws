#--------------------------------------------------------------
# This module creates all networking resources
#--------------------------------------------------------------

variable "name"            { }
variable "azs"             { }
variable "region"          { }
variable "vpc_cidr"        { }
variable "public_subnets"  { }
variable "key_name"        { }
variable "ib_private_subnets" { }
variable "cdi_private_subnets" { }
variable "solr_private_subnets" { }
variable "nat_ami"              { }
variable "nat_instance_type"    { }


module "vpc" {
  source = "./vpc"

  name = "${var.name}"
  cidr = "${var.vpc_cidr}"
}

module "public_subnet" {
  source = "./public_subnet"

  name   = "${var.name}-public"
  vpc_id = "${module.vpc.vpc_id}"
  cidrs  = "${var.public_subnets}"
  azs    = "${var.azs}"
}

module "nat" {
  source = "./nat"
  name                = "${var.name}-nat"
  vpc_id              = "${module.vpc.vpc_id}"
  vpc_cidr            = "${module.vpc.vpc_cidr}"
  key_name            = "${var.key_name}"
  subnet_ids          = "${module.public_subnet.subnet_ids}"
  ami                 = "${var.nat_ami}"
  instance_type       = "${var.nat_instance_type}"
}

module "ib_private_subnet" {
  source = "./private_subnet"

  name   = "${var.name}-ib-prv"
  vpc_id = "${module.vpc.vpc_id}"
  cidrs  = "${var.ib_private_subnets}"
  azs    = "${var.azs}"
  nat_gateway_ids = "${module.nat.nat_id}"
}

module "cdi_private_subnet" {
  source = "./private_subnet"

  name   = "${var.name}-cdi-prv"
  vpc_id = "${module.vpc.vpc_id}"
  cidrs  = "${var.cdi_private_subnets}"
  azs    = "${var.azs}"
  nat_gateway_ids = "${module.nat.nat_id}"
}

module "solr_private_subnet" {
  source = "./private_subnet"

  name   = "${var.name}-solr-prv"
  vpc_id = "${module.vpc.vpc_id}"
  cidrs  = "${var.solr_private_subnets}"
  azs    = "${var.azs}"
  nat_gateway_ids = "${module.nat.nat_id}"
}


resource "aws_network_acl" "acl" {
  vpc_id     = "${module.vpc.vpc_id}"
  subnet_ids = ["${concat(split(",", module.ib_private_subnet.subnet_ids),
        split(",", module.cdi_private_subnet.subnet_ids),
        split(",", module.solr_private_subnet.subnet_ids))}"]

  ingress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
  egress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags { Name = "${var.name}-service-all" }
}


# VPC
output "vpc_id"   { value = "${module.vpc.vpc_id}" }
output "vpc_cidr" { value = "${module.vpc.vpc_cidr}" }

# Subnets
output "public_subnet_ids"  { value = "${module.public_subnet.subnet_ids}" }
output "ib_subnet_ids" { value = "${module.ib_private_subnet.subnet_ids}" }
output "cdi_subnet_ids" { value = "${module.cdi_private_subnet.subnet_ids}" }
output "solr_subnet_ids" { value = "${module.solr_private_subnet.subnet_ids}" }

# NAT
output "nat_gateway_ids" { value = "${module.nat.nat_id}" }
