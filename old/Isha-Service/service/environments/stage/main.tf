variable "azs"                { }
variable "name"               { }
variable "region"             { }
variable "sub_domain"         { }
variable "key_name"           { }
variable "env"                { }
variable "service_vpc_cidr"   { }
variable "api_subnet_cidrs" { }
variable "service_subnet_cidrs" { }
variable "cache_subnet_cidrs"   { }
variable "rds_subnet_cidrs"  { }
variable "api_ami"            { }
variable "api_instance_type"  { }
variable "service_ami"        { }
variable "service_instance_type"    { }
variable "cache_ami"        { }
variable "cache_instance_type"    { }
variable "nat_ami"            { }
variable "nat_instance_type"  { }
variable "rds_db_name"  { }
variable "rds_instance_type"  { }
variable "rds_username"  { }
variable "rds_password"  { }
variable "parameter_group_name"  { }
variable "deploy_ami"                  { }
variable "deploy_instance_type"        { }


provider "aws" {
  region = "${var.region}"
  access_key = "AKIAIBVWJ6R7IYGOWIMQ"
  secret_key = "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"
}

module "network" {
  source = "../../modules/network"

  name                  = "${var.name}-${var.env}"
  azs                   = "${var.azs}"
  region                = "${var.region}"
  key_name              = "${var.key_name}"
  vpc_cidr              = "${var.service_vpc_cidr}"
  api_subnet_cidrs      = "${var.api_subnet_cidrs}"
  service_subnet_cidrs    = "${var.service_subnet_cidrs}"
  cache_subnet_cidrs      = "${var.cache_subnet_cidrs}"
  nat_ami               = "${var.nat_ami}"
  nat_instance_type     = "${var.nat_instance_type}"
  rds_subnet_cidrs        = "${var.rds_subnet_cidrs}"
}

module "service" {
  source = "../../modules/service"

  name                    = "${var.name}-${var.env}"
  vpc_id                  = "${module.network.vpc_id}"
  rds_db_name             = "${var.rds_db_name}"
  rds_instance_type       = "${var.rds_instance_type}"
  rds_username            = "${var.rds_username}"
  rds_password            = "${var.rds_password}"
  rds_subnet_group_name   = "${module.network.rds_subnet_group_name}"
  rds_access_cidr         = "${var.service_subnet_cidrs},${var.cache_subnet_cidrs}"
}

module "compute" {
  source = "../../modules/compute"

  name                = "${var.name}-${var.env}"
  region              = "${var.region}"
  azs                 = "${var.azs}"
  env                 = "${var.env}"
  key_name            = "${var.key_name}"
  vpc_id              = "${module.network.vpc_id}"
  vpc_cidr            = "${module.network.vpc_cidr}"
  api_ami              = "${var.api_ami}"
  api_instance_type    = "${var.api_instance_type}"
  api_subnet_ids       = "${module.network.api_subnet_ids}"
  service_ami             = "${var.service_ami}"
  service_instance_type   = "${var.service_instance_type}"
  service_subnet_ids      = "${module.network.service_subnet_ids}"
  cache_ami             = "${var.cache_ami}"
  cache_instance_type   = "${var.cache_instance_type}"
  cache_subnet_ids      = "${module.network.cache_subnet_ids}"
  deploy_ami             = "${var.deploy_ami}"
  deploy_instance_type   = "${var.deploy_instance_type}"
  rds_url               =  "${module.service.rds_url}"
  api_subnet_cidrs       = "${var.api_subnet_cidrs}"
  service_subnet_cidrs    = "${var.service_subnet_cidrs}"
  rds_super_user      = "${var.rds_username}"
  rds_password        = "${var.rds_password}"
  monitor_host_ip	   = "${module.network.monitor_host_ip}"
}


output "rds_url" { value = "${module.service.rds_url}" }


