#--------------------------------------------------------------
# This module creates all resources necessary for service Layer
#--------------------------------------------------------------

variable "name"              { default = "service" }
variable "vpc_id"            { }
variable "vpc_cidr"          { }
variable "key_name"          { }
variable "subnet_ids"        { }
variable "ami"               { }
variable "instance_type"     { }
variable "azs"               { }
variable "env"               { }
variable "rds_url"           { }
variable "rds_super_user"           { }
variable "rds_password"           { }
variable "redis_ips"         { }
variable "solr_ips"          { }
variable "api_subnet_cidrs" { }
variable "monitor_host_ip" { }


resource "aws_security_group" "service" {
  name        = "${var.name}-security_group"
  vpc_id      = "${var.vpc_id}"
  description = "service security group"

  tags      {
    Name = "${var.name}-security_group"
  }
  lifecycle { create_before_destroy = true }

  ingress {
    protocol    = "tcp"
    from_port   = 8080
    to_port     = 8080
    cidr_blocks = ["${var.api_subnet_cidrs}"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["${var.vpc_cidr}"]
  }
  ingress {
    protocol    = "tcp"
    from_port   = 5666
    to_port     = 5666
    cidr_blocks = ["${var.monitor_host_ip}/31"]
  }
  ingress {
    protocol    = "icmp"
    from_port   = 8
    to_port     = -1
    cidr_blocks = ["${var.monitor_host_ip}/31"]
  }
  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}


# Render a part using a `template_file`
data "template_file" "init" {
  template = "${file("${path.module}/service.sh.tpl")}"

  vars {
    env = "${var.env}"
    host_name = "service${count.index}"
    monitor_host_ip = "${var.monitor_host_ip}"
    rds_url = "${var.rds_url}"
    redis_url = "${var.redis_ips}"
    solr_url = "${var.solr_ips}"
    rds_super_user = "${var.rds_super_user}"
    rds_password = "${var.rds_password}"
  }
}

resource "aws_instance" "service" {
  ami           = "${element(split(",", var.ami), count.index)}"
  instance_type = "${var.instance_type}"
  key_name      = "${var.key_name}"
  subnet_id     = "${element(split(",", var.subnet_ids), count.index)}"
  user_data     = "${element(data.template_file.init.*.rendered, count.index)}"
  vpc_security_group_ids = ["${aws_security_group.service.id}"]
  tags      { Name = "${var.name}" }
  lifecycle { create_before_destroy = true }
}

output "public_ips"   { value = "${join(",", aws_instance.service.*.public_ip)}" }
output "private_ips"  { value = "${join(",", aws_instance.service.*.private_ip)}" }

