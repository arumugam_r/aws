#!/bin/bash -ex


echo get the instance-id from meta-data.
IP=`wget http://169.254.169.254/latest/meta-data/local-ipv4  -qO - | cut -d . -f 1,2,3,4`

echo name variable for the hostname $IP
HOSTNAME=${host_name}
MONITOR=${monitor_host_ip}

echo update the hostname file with the new hostname/fqdn  $HOSTNAME.ishacloud.org
echo $HOSTNAME.ishacloud.org > /etc/hostname

echo update the running hostname
hostname $HOSTNAME.ishacloud.org

echo Crate hosts file with Hostname and IP
echo "127.0.0.1 localhost" > /etc/hosts
echo $IP  $HOSTNAME.ishacloud.org >> /etc/hosts

echo Add monitor hosts ip in the /etc/hosts file
echo $MONITOR  monitor.ishacloud.org >> /etc/hosts

#---------------------------------- Nagios NRPE Plugin Configuration Start -------------------------------

sed -i '/allowed_hosts/c\allowed_hosts=127.0.0.1,monitor.ishacloud.org' /etc/nagios/nrpe.cfg

#---------------------------------- Nagios NRPE Plugin Configuration End -------------------------------



#---------------------------------- NGINX Configuration Start -------------------------------
rm -rf /appdata/deployables
mkdir /appdata/deployables && cd /appdata/deployables

echo "Temp directories are created."

echo "Downloading & Setting up the certificates ca.crt, ishadontin.key and ishadotin.pem"

wget https://s3.amazonaws.com/isha-bucket/${env}/artifactory/latest/api/ca.crt  > /dev/null 2>&1
mkdir -p /etc/ssl/Isha/certs
mv ca.crt /etc/ssl/Isha/certs/ca.crt
chmod 644 /etc/ssl/Isha/certs/ca.crt

wget https://s3.amazonaws.com/isha-bucket/${env}/artifactory/latest/api/ishadotin.key  > /dev/null 2>&1
mkdir -p /etc/nginx/Isha/cert
mv ishadotin.key /etc/nginx/Isha/cert/ishadotin.key
chmod 644 /etc/nginx/Isha/cert/ishadotin.key

wget https://s3.amazonaws.com/isha-bucket/${env}/artifactory/latest/api/ishadotin.pem  > /dev/null 2>&1
mv ishadotin.pem /etc/nginx/Isha/cert/ishadotin.pem
chmod 644 /etc/nginx/Isha/cert/ishadotin.pem

echo "Downloading & Setting up  the NGINX Config ib.conf."
wget https://s3.amazonaws.com/isha-bucket/${env}/artifactory/latest/api/ib.conf  > /dev/null 2>&1
rm -f /etc/nginx/conf.d/default.conf
mv ib.conf /etc/nginx/conf.d/ib.conf
chmod 644  /etc/nginx/conf.d/ib.conf

wget https://s3.amazonaws.com/isha-bucket/${env}/artifactory/latest/api/merge-ui.tar  > /dev/null 2>&1
wget https://s3.amazonaws.com/isha-bucket/${env}/artifactory/latest/api/widget.tar > /dev/null 2>&1

# Clear the previous files
rm -rf /appdata/ui

#Unwrap and deploy it to nginx directories.
mkdir -p /appdata/ui

tar -xvf merge-ui.tar -C  /appdata/ui
tar -xvf widget.tar -C  /appdata/ui

#Directories permission changes.
chown -R nginx:root /appdata/ui && chmod -R 755 /appdata/ui

echo replacing Service Layer IPaddress ${service_ips}
sed -i 's/<service_url>/${service_ips}/g' /etc/nginx/conf.d/ib.conf

#---------------------------------- NGINX End Start -------------------------------

echo Restarting NGINX server.
service nginx restart

exit 0

