#--------------------------------------------------------------
# Service Layer VPC Setup
#--------------------------------------------------------------


name              = "isha-srv"
region            = "us-east-1"
#only one AZ now?
azs               = "us-east-1e,us-east-1c" # AZs are region specific

#--------------------------------------------------------------
# Network
#--------------------------------------------------------------

key_name = "Isha-Service"

#--------------------------------------------------------------
# EC2 Instances
#--------------------------------------------------------------
api_instance_type       = "t2.micro"
api_ami                 = "ami-2f97d738"

service_instance_type   = "t2.small"
service_ami             = "ami-7c95d56b"

deploy_instance_type   	= "t2.small"
deploy_ami             	= "ami-04b7f713"


cache_instance_type     = "t2.small"
cache_ami               = "ami-ab3e75bc"

#--------------------------------------------------------------
# NAT Instance instead of NAT gateway.
#--------------------------------------------------------------

nat_instance_type       = "t2.micro"
nat_ami                 = "ami-0d94d41a"

#--------------------------------------------------------------
# RDS Instances
#--------------------------------------------------------------
rds_db_name             = "ISHAServiceDB"
rds_instance_type       = "db.m3.medium"
