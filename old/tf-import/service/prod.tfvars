#--------------------------------------------------------------
# Service Layer VPC Setup
#--------------------------------------------------------------

env               = "prod"
sub_domain        = "ishacloud.org"

#--------------------------------------------------------------
# Network
#--------------------------------------------------------------

service_vpc_cidr        = "10.0.32.0/20"

api_subnet_cidrs       = "10.0.32.0/24"               # Creating one public subnet only on one AZ for NAT
service_subnet_cidrs    = "10.0.34.0/24"              # Creating one private subnet per AZ
cache_subnet_cidrs      = "10.0.36.0/24"              # Creating one private subnet per AZ
rds_subnet_cidrs        = "10.0.38.0/24,10.0.39.0/24" # Creating one private subnet per AZ

#--------------------------------------------------------------
# RDS
#--------------------------------------------------------------
rds_username            = "ishasrvprod"
rds_password            = "isha_srv_it_230"
parameter_group_name    = "rds_prod_parameter_group"
