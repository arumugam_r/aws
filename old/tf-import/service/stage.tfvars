#--------------------------------------------------------------
# Service Layer VPC Setup
#--------------------------------------------------------------

env               = "stage"
sub_domain        = "isha.in"

#--------------------------------------------------------------
# Network
#--------------------------------------------------------------

service_vpc_cidr        = "192.168.32.0/20"

api_subnet_cidrs       = "192.168.32.0/24"               # Creating one public subnet only on one AZ for NAT
service_subnet_cidrs    = "192.168.34.0/24"              # Creating one private subnet per AZ
cache_subnet_cidrs      = "192.168.36.0/24"              # Creating one private subnet per AZ
rds_subnet_cidrs        = "192.168.38.0/24,192.168.39.0/24" # Creating one private subnet per AZ

#--------------------------------------------------------------
# RDS
#--------------------------------------------------------------
rds_username            = "ishaservice"
rds_password            = "isha_srv_007"
parameter_group_name    = "rds_stage_parameter_group"
