#--------------------------------------------------------------
# This module creates all networking resources
#--------------------------------------------------------------

variable "name"            { }
variable "azs"             { }
variable "region"          { }
variable "vpc_cidr"        { }
variable "key_name"        { }
variable "api_subnet_cidrs" { }
variable "service_subnet_cidrs" { }
variable "cache_subnet_cidrs" { }
variable "nat_ami"              { }
variable "nat_instance_type"    { }
variable "rds_subnet_cidrs"    { }


module "vpc" {
  source = "./vpc"

  name = "${var.name}"
  cidr = "${var.vpc_cidr}"
}

module "nat" {
  source = "./nat"
  name                = "${var.name}-nat"
  vpc_id              = "${module.vpc.vpc_id}"
  vpc_cidr            = "${module.vpc.vpc_cidr}"
  key_name            = "${var.key_name}"
  subnet_ids          = "${module.api_public_subnet.subnet_ids}"
  ami                 = "${var.nat_ami}"
  instance_type       = "${var.nat_instance_type}"
}


module "api_public_subnet" {
  source = "./public_subnet"

  name   = "${var.name}-api-pub"
  vpc_id = "${module.vpc.vpc_id}"
  cidrs  = "${var.api_subnet_cidrs}"
  azs    = "${var.azs}"
}

module "service_prv_subnet" {
  source = "./private_subnet"

  name   = "${var.name}-service-prv"
  vpc_id = "${module.vpc.vpc_id}"
  cidrs  = "${var.service_subnet_cidrs}"
  azs    = "${var.azs}"
  nat_gateway_ids = "${module.nat.nat_id}"
}

module "cache_prv_subnet" {
  source = "./private_subnet"

  name   = "${var.name}-cache-prv"
  vpc_id = "${module.vpc.vpc_id}"
  cidrs  = "${var.cache_subnet_cidrs}"
  azs    = "${var.azs}"
  nat_gateway_ids = "${module.nat.nat_id}"
}


module "rds_subnet" {
  source = "./rds_subnet"

  name   = "${var.name}-rds"
  vpc_id = "${module.vpc.vpc_id}"
  cidrs  = "${var.rds_subnet_cidrs}"
  azs    = "${var.azs}"

}


resource "aws_network_acl" "acl" {
  vpc_id     = "${module.vpc.vpc_id}"
  subnet_ids = ["${concat(split(",", module.api_public_subnet.subnet_ids),
      split(",", module.service_prv_subnet.subnet_ids),
      split(",", module.cache_prv_subnet.subnet_ids),
      split(",", module.rds_subnet.subnet_ids))}"]

  ingress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
  egress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags { Name = "${var.name}-service-all" }
}


# VPC
output "vpc_id"   { value = "${module.vpc.vpc_id}" }
output "vpc_cidr" { value = "${module.vpc.vpc_cidr}" }

# Subnets
output "api_subnet_ids" { value = "${module.api_public_subnet.subnet_ids}" }
output "service_subnet_ids" { value = "${module.service_prv_subnet.subnet_ids}" }
output "cache_subnet_ids" { value = "${module.cache_prv_subnet.subnet_ids}" }

# RDS
output "rds_subnet_group_name" { value = "${module.rds_subnet.rds_subnet_group_name}" }

# NAT
output "nat_gateway_ids" { value = "${module.nat.nat_id}" }
output "monitor_host_ip" { value = "${module.nat.monitor_host_ip}" }

