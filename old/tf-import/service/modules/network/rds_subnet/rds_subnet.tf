#--------------------------------------------------------------
# This module creates all resources necessary for a public
# subnet
#--------------------------------------------------------------

variable "name"   { default = "isha-rds" }
variable "vpc_id" { }
variable "cidrs"  { }
variable "azs"    { }

resource "aws_subnet" "rds_subnet" {
  vpc_id            = "${var.vpc_id}"
  count             = "${length(split(",", var.cidrs))}"
  cidr_block        = "${element(split(",", var.cidrs), count.index)}"
  availability_zone = "${element(split(",", var.azs), count.index)}"
  tags      { Name = "${var.name}.${element(split(",", var.azs), count.index)}" }
  lifecycle { create_before_destroy = true }
}

resource "aws_db_subnet_group" "rds_subnet_group" {
  name            = "${var.name}-subnet_group"
  description     = "Rds Subnet Group"
  subnet_ids     = ["${aws_subnet.rds_subnet.*.id}"]
  tags {
      Name = "${var.name} DB subnet group"
  }
}

output "rds_subnet_group_name" {
  value = "${aws_db_subnet_group.rds_subnet_group.name}"
}

output "subnet_ids" { value = "${join(",", aws_subnet.rds_subnet.*.id)}" }