#!/bin/bash -ex

echo get the instance-id from meta-data.
IP=`wget http://169.254.169.254/latest/meta-data/local-ipv4  -qO - | cut -d . -f 1,2,3,4`

echo name variable for the hostname $IP
HOSTNAME=${host_name}
MONITOR=${monitor_host_ip}

echo update the hostname file with the new hostname/fqdn  $HOSTNAME.ishacloud.org
echo $HOSTNAME.ishacloud.org > /etc/hostname

echo update the running hostname
hostname $HOSTNAME.ishacloud.org

echo Crate hosts file with Hostname and IP
echo "127.0.0.1 localhost" > /etc/hosts
echo $IP  $HOSTNAME.ishacloud.org >> /etc/hosts

echo Add monitor hosts ip in the /etc/hosts file
echo $MONITOR  monitor.ishacloud.org >> /etc/hosts

#---------------------------------- Nagios NRPE Plugin Configuration Start -------------------------------

sed -i '/allowed_hosts/c\allowed_hosts=127.0.0.1,monitor.ishacloud.org' /etc/nagios/nrpe.cfg

#---------------------------------- Nagios NRPE Plugin Configuration End -------------------------------


#---------------------------------- SOLR & NGINX Configuration Start -------------------------------

db_host="${rds_url}"
db_port="5432"

thishost="$IP"

# initialize the cdi db here
db_user="mdm_server";
db_pass="mdmServer";
db_name="cdi_server_db";

export db_super_user="${rds_super_user}";
export PGPASSWORD="${rds_password}";


createDB() {

	var_db_name=$1;
	var_db_owner=$2;

	db=`echo "select count(*) from pg_catalog.pg_database where datname='$var_db_name'" | psql -U $db_super_user -h $db_host postgres -t`
	echo "#$db found with name : $var_db_name"
	db=`expr $db + 0`
	if [ $db -ne 0 ]
	then
		echo "No need to create the DB"
	else
		echo "DB needs to be created"
		echo "create database $var_db_name owner $db_super_user;"| psql -U $db_super_user  -h $db_host postgres -t
		echo "alter database $var_db_name owner to $var_db_owner;"| psql -U $db_super_user  -h $db_host postgres -t
	fi

}

createRole() {

	var_db_user=$1;
	var_db_user_pass=$2;

	role=`echo "SELECT count(*) FROM pg_roles WHERE rolname ='$db_user'"| psql -U $db_super_user -h $db_host postgres -t`;
	echo "#$role found with name : $var_db_user"
	role=`expr $role + 0`
	if [ $role -ne 0 ]
	then
		echo "No need to create the role"
	else
		echo "Role needs to be created"
		echo "create role $var_db_user with password '$var_db_user_pass' login;"| psql -U $db_super_user -h $db_host postgres -t
	fi

}

createRole $db_user $db_pass
createDB $db_name $db_user


rm -rf  /appdata/deployables
rm -rf  /appdata/solr-artifacts/*
mkdir /appdata/deployables && cd /appdata/deployables

#Copying the solr configuration from S3 for updating.
wget https://s3.amazonaws.com/isha-bucket/${env}/artifactory/latest/cache/solr-conf.tar.gz > /dev/null 2>&1

tar -xzf /appdata/deployables/solr-conf.tar.gz -C /appdata/solr-artifacts/

cd /appdata/solr-artifacts/solr
sed -i "s/__host/$thishost/g" solr.in.sh
sed -i "s/__thishost/$thishost/g" solr.in.sh
cp -r solr.in.sh  /etc/default/solr.in.sh
chown -R solr:solr /etc/default/solr.in.sh

sed -i "s/__host/$thishost/g" zoo.cfg
cp -r zoo.cfg /etc/zookeeper/conf/zoo.cfg
chmod 644 /etc/zookeeper/conf/zoo.cfg

#Setting the zk id file
echo "1" > /etc/zookeeper/conf/myid
chmod 644 /etc/zookeeper/conf/myid
echo "restarting zookeeper"
service zookeeper restart

# change the db related params
echo "initializing the solr-conf files"
sed -i "s/__rds_host/$db_host/g" conf/db-data-config.xml
sed -i "s/__rds_port/$db_port/g" conf/db-data-config.xml
sed -i "s/__cdi_database/$db_name/g" conf/db-data-config.xml
sed -i "s/__rds_cdi_user_name/$db_user/g" conf/db-data-config.xml
sed -i "s/__rds_cdi_password/$db_pass/g" conf/db-data-config.xml

cd -;

ZKHOST="$thishost:2181"

echo "updating the zookeeper with the solr-conf"
sh /appdata/solr/server/scripts/cloud-scripts/zkcli.sh -zkhost $ZKHOST/solr -cmd upconfig -confname cdi-search -confdir /appdata/solr-artifacts/solr/conf

echo "Restarting the solr service"
service solr restart

# Need to create redis log directory otherwise server will fail

REDIS_LOG_DIRECTORY="/var/log/redis"
if [ ! -d $REDIS_LOG_DIRECTORY ]; then
    mkdir $REDIS_LOG_DIRECTORY
fi


chown redis: /var/log/redis/
chmod 774 /var/log/redis/


echo "Initializing Redis"
wget https://s3.amazonaws.com/isha-bucket/${env}/artifactory/latest/cache/redis.conf > /dev/null 2>&1
cp redis.conf /etc/redis/redis.conf
echo "Restarting redis-server"
service redis-server restart

echo "Sleep 10sec before executing the delta import"
sleep 10

# Delta import for solr instance as it bootsup everytime
echo "Performing delta-import on Solr"
curl http://localhost:8983/solr/cdi-search/dataimport?command=delta-import

#---------------------------------- SOLR & NGINX Configuration End -------------------------------


exit 0

