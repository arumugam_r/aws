#!/bin/bash -ex

echo get the instance-id from meta-data.
IP=`wget http://169.254.169.254/latest/meta-data/local-ipv4  -qO - | cut -d . -f 1,2,3,4`

echo name variable for the hostname $IP
HOSTNAME=${host_name}
MONITOR=${monitor_host_ip}

echo update the hostname file with the new hostname/fqdn  $HOSTNAME.ishacloud.org
echo $HOSTNAME.ishacloud.org > /etc/hostname

echo update the running hostname
hostname $HOSTNAME.ishacloud.org

echo Crate hosts file with Hostname and IP
echo "127.0.0.1 localhost" > /etc/hosts
echo $IP  $HOSTNAME.ishacloud.org >> /etc/hosts


echo Add monitor hosts ip in the /etc/hosts file
echo $MONITOR  monitor.ishacloud.org >> /etc/hosts

#---------------------------------- Nagios NRPE Plugin Configuration Start -------------------------------

sed -i '/allowed_hosts/c\allowed_hosts=127.0.0.1,monitor.ishacloud.org' /etc/nagios/nrpe.cfg

#---------------------------------- Nagios NRPE Plugin Configuration End -------------------------------



export db_super_user="${rds_super_user}";
export PGPASSWORD="${rds_password}";

db_host="${rds_url}"
db_port="5432"
db_user="mdm_server";
db_pass="mdmServer";

solr_host="${solr_url}";
solr_port="8983";

redis_host="${redis_url}";
redis_port="6379";

LOCAL_HOST="$IP"

tomcat_dir="/appdata/apache-tomcat-8.0.21"
deployables_home="/appdata/deployables"
#------------Props------------------------

# For auth
key_auth_db_host="db.host"
key_auth_db_port="db.port"
key_auth_db_name="db.database"
key_auth_db_user="db.user";
key_auth_db_password="db.password";

val_auth_db_name="isha_auth"
val_auth_db_user=$db_user;
val_auth_db_password=$db_pass;
val_auth_db_host=$db_host
val_auth_db_port=$db_port

# For CDI
key_cdi_db_host="db.host"
key_cdi_db_port="db.port"
key_cdi_db_name="db.database"
key_cdi_db_user="db.user";
key_cdi_db_password="db.password";
key_cdi_solr_host="solr.host"
key_cdi_solr_port="solr.port"
key_cdi_redis_host="redis.host"
key_cdi_redis_port="redis.port"

val_cdi_db_name="cdi_server_db"
val_cdi_db_user=$db_user;
val_cdi_db_password=$db_pass;
val_cdi_db_host=$db_host
val_cdi_db_port=$db_port
val_cdi_solr_host="$solr_host:2181/solr"
val_cdi_solr_port=$solr_port
val_cdi_redis_host=$redis_host
val_cdi_redis_port=$redis_port

# For ib-cdi
key_ib_cdi_db_host="db.host"
key_ib_cdi_db_port="db.port"
key_ib_cdi_db_name="db.database"
key_ib_cdi_db_user="db.user";
key_ib_cdi_db_password="db.password";
key_ib_cdi_redis_host="redis.host"
key_ib_cdi_redis_port="redis.port"
key_ib_cdi_ibcdibaseurl="ibCdiBaseUrl"
key_ib_cdi_cdibaseurl="cdiBaseUrl"


val_ib_cdi_db_name="ib_database"
val_ib_cdi_db_user=$db_user;
val_ib_cdi_db_password=$db_pass;
val_ib_cdi_db_host=$db_host
val_ib_cdi_db_port=$db_port
val_ib_cdi_redis_host=$redis_host
val_ib_cdi_redis_port=$redis_port
val_ib_cdi_ibcdibaseurl="http://$LOCAL_HOST:8080/ib-cdi/api/"
val_ib_cdi_cdibaseurl="http://$LOCAL_HOST:8080/IshaMDMService/api/"


# For ib-independent

key_ib_independent_db_host="db.host"
key_ib_independent_db_port="db.port"
key_ib_independent_db_name="db.database"
key_ib_independent_db_user="db.user";
key_ib_independent_db_password="db.password";
key_ib_independent_redis_host="redis.host"
key_ib_independent_redis_port="redis.port"
key_ib_independent_ibcdibaseurl="ibCdiBaseUrl"
key_ib_independent_cdibaseurl="cdiBaseUrl"
key_ib_independent_authurl="ishaAuth.baseUrl"


val_ib_independent_db_name="ib_database"
val_ib_independent_db_user=$db_user;
val_ib_independent_db_password=$db_pass;
val_ib_independent_db_host=$db_host
val_ib_independent_db_port=$db_port
val_ib_independent_redis_host=$redis_host
val_ib_independent_redis_port=$redis_port
val_ib_independent_ibcdibaseurl="http://$LOCAL_HOST:8080/ib-cdi/api/"
val_ib_independent_cdibaseurl="http://$LOCAL_HOST:8080/IshaMDMService/api/"
val_ib_independent_authurl="http://$LOCAL_HOST:8080/auth/api"




createDB() {

	var_db_name=$1;
	var_db_owner=$2;

	db=`echo "select count(*) from pg_catalog.pg_database where datname='$var_db_name'" | psql -U $db_super_user -h $db_host postgres -t`
	echo "#$db found with name : $var_db_name"
	db=`expr $db + 0`
	if [ $db -ne 0 ]
	then
		echo "No need to create the DB"
	else
		echo "DB needs to be created"
		echo "create database $var_db_name owner $db_super_user;"| psql -U $db_super_user  -h $db_host postgres -t
		echo "alter database $var_db_name owner to $var_db_owner;"| psql -U $db_super_user  -h $db_host postgres -t
	fi

}

createRole() {

	var_db_user=$1;
	var_db_user_pass=$2;

	role=`echo "SELECT count(*) FROM pg_roles WHERE rolname ='$db_user'"| psql -U $db_super_user -h $db_host postgres -t`;
	echo "#$role found with name : $var_db_user"
	role=`expr $role + 0`
	if [ $role -ne 0 ]
	then
		echo "No need to create the role"
	else
		echo "Role needs to be created"
		echo "create role $var_db_user with password '$var_db_user_pass' login;"| psql -U $db_super_user -h $db_host postgres -t
	fi

}

updateProperty() {
	ss=$1;
	rs=$2;
	fn=$3;

	ss=`echo $ss|sed -e 's/\"/\\\"/g'`
	rs=`echo $rs|sed -e 's/\"/\\\"/g'`

	cat $fn|awk "BEGIN{FS=\"=\";vss=\"$ss\";vrs=\"$rs\"}{if(\$1==vss){print \$1\"=\"vrs}else print \$0;}" > __temp
	mv __temp $fn
}

#--------------Create DBs and User---------------------
createRole $db_user $db_pass
createDB "ib_database" $db_user
createDB "cdi_server_db" $db_user
createDB "isha_auth" $db_user

#------------Create the temporary directories---------
cd /appdata
rm -rf  $deployables_home
mkdir $deployables_home && cd $deployables_home


#------------------------------
# Create the tables for each DB
#------------------------------

mkdir db-migration;
cd db-migration;

export PATH=/appdata/dbsteward/composer/vendor/bin:$PATH

# get the db-migration script
wget 'https://s3.amazonaws.com/isha-bucket/${env}/artifactory/latest/service/db-migration/db-migration.sh' > /dev/null 2>&1
sed -i "s/__db_host/$db_host/g" db-migration.sh
sed -i "s/__db_port/$db_port/g" db-migration.sh
sed -i "s/__db_user/$db_user/g" db-migration.sh
sed -i "s/__db_password/$db_pass/g" db-migration.sh

export PGPASSWORD="$db_pass";

# perform db-migration for auth db
mkdir isha_auth;
wget 'https://s3.amazonaws.com/isha-bucket/${env}/artifactory/latest/service/db-migration/auth-db-schema.xml' > /dev/null 2>&1
mv auth-db-schema.xml isha_auth/;
sh db-migration.sh 'isha_auth' 'auth-db-schema.xml';

# perform db-migration for ib database
mkdir ib_database;
wget 'https://s3.amazonaws.com/isha-bucket/${env}/artifactory/latest/service/db-migration/ib-db-schema.xml' > /dev/null 2>&1
mv ib-db-schema.xml ib_database/;
sh db-migration.sh 'ib_database' 'ib-db-schema.xml';

# perform db-migration for cdi database
mkdir cdi_server_db;
wget 'https://s3.amazonaws.com/isha-bucket/${env}/artifactory/latest/service/db-migration/cdi-db-schema.xml' > /dev/null 2>&1
mv cdi-db-schema.xml cdi_server_db/;
sh db-migration.sh 'cdi_server_db' 'cdi-db-schema.xml';


# moving back to the root of the temp dir
cd $deployables_home;


#------------Deploy the WARs--------------------
echo "MDM Deployment start."
echo "Stopping tomcat"
service tomcat stop;

cd $deployables_home;
rm -rf temp;
mkdir temp;
cd temp;
echo "Downloading the IshaMDMService.war."
wget https://s3.amazonaws.com/isha-bucket/${env}/artifactory/latest/service/IshaMDMService.war > /dev/null 2>&1

echo "Unjar the artifacts."
jar -xvf IshaMDMService.war > /dev/null 2>&1

updateProperty "$key_cdi_db_host" "$val_cdi_db_host" 'WEB-INF/classes/config/mdm-dao.properties'
updateProperty "$key_cdi_db_port" "$val_cdi_db_port" 'WEB-INF/classes/config/mdm-dao.properties'
updateProperty "$key_cdi_db_name" "$val_cdi_db_name" 'WEB-INF/classes/config/mdm-dao.properties'
updateProperty "$key_cdi_db_user" "$val_cdi_db_user" 'WEB-INF/classes/config/mdm-dao.properties'
updateProperty "$key_cdi_db_password" "$val_cdi_db_password" 'WEB-INF/classes/config/mdm-dao.properties'
updateProperty "$key_cdi_solr_host" "$val_cdi_solr_host" 'WEB-INF/classes/config/mdm-dao.properties'
updateProperty "$key_cdi_solr_port" "$val_cdi_solr_port" 'WEB-INF/classes/config/mdm-dao.properties'
updateProperty "$key_cdi_redis_host" "$val_cdi_redis_host" 'WEB-INF/classes/config/mdm-dao.properties'
updateProperty "$key_cdi_redis_port" "$val_cdi_redis_port" 'WEB-INF/classes/config/mdm-dao.properties'

rm IshaMDMService.war;
jar -cvf IshaMDMService.war * > /dev/null 2>&1
rm -rf $tomcat_dir/webapps/IshaMDMService*;
mv IshaMDMService.war $tomcat_dir/webapps/
echo "MDM deployment done."

#-----------Deploy Auth--------------------
echo "auth.war deployment start"
cd $deployables_home
rm -rf temp;
mkdir temp && cd temp;

echo "Downloading the Auth.war."
wget https://s3.amazonaws.com/isha-bucket/${env}/artifactory/latest/service/auth.war > /dev/null 2>&1

echo "Unjar the artifacts."
jar -xvf auth.war > /dev/null 2>&1

updateProperty "$key_auth_db_host" "$val_auth_db_host" 'WEB-INF/classes/config/mdm-dao.properties'
updateProperty "$key_auth_db_port" "$val_auth_db_port" 'WEB-INF/classes/config/mdm-dao.properties'
updateProperty "$key_auth_db_name" "$val_auth_db_name" 'WEB-INF/classes/config/mdm-dao.properties'
updateProperty "$key_auth_db_user" "$val_auth_db_user" 'WEB-INF/classes/config/mdm-dao.properties'
updateProperty "$key_auth_db_password" "$val_auth_db_password" 'WEB-INF/classes/config/mdm-dao.properties'

rm auth.war;
jar -cvf auth.war *  > /dev/null 2>&1
rm -rf $tomcat_dir/webapps/auth*;
mv auth.war $tomcat_dir/webapps/
echo "auth.war deployed."

#--------------Deploy ib-cdi--------------------------------

echo "ib-cdi.war deployment start"
cd $deployables_home
rm -rf temp;
mkdir temp && cd temp;

echo "Downloading the ib-cdi.war."
wget https://s3.amazonaws.com/isha-bucket/${env}/artifactory/latest/service/ib-cdi.war > /dev/null 2>&1

echo "Unjar the artifacts."
jar -xvf ib-cdi.war > /dev/null 2>&1

#-----------------
# ./WEB-INF/classes/framework/config/service.properties
updateProperty "$key_ib_cdi_db_host" "$val_ib_cdi_db_host" 'WEB-INF/classes/framework/config/service.properties'
updateProperty "$key_ib_cdi_db_port" "$val_ib_cdi_db_port" 'WEB-INF/classes/framework/config/service.properties'
updateProperty "$key_ib_cdi_db_name" "$val_ib_cdi_db_name" 'WEB-INF/classes/framework/config/service.properties'
updateProperty "$key_ib_cdi_db_user" "$val_ib_cdi_db_user" 'WEB-INF/classes/framework/config/service.properties'
updateProperty "$key_ib_cdi_db_password" "$val_ib_cdi_db_password" 'WEB-INF/classes/framework/config/service.properties'
updateProperty "$key_ib_cdi_redis_host" "$val_ib_cdi_redis_host" 'WEB-INF/classes/framework/config/service.properties'
updateProperty "$key_ib_cdi_redis_port" "$val_ib_cdi_redis_port" 'WEB-INF/classes/framework/config/service.properties'
updateProperty "$key_ib_cdi_ibcdibaseurl" "$val_ib_cdi_ibcdibaseurl" 'WEB-INF/classes/framework/config/service.properties'
updateProperty "$key_ib_cdi_cdibaseurl" "$val_ib_cdi_cdibaseurl" 'WEB-INF/classes/framework/config/service.properties'
# ./WEB-INF/classes/config/service.properties
updateProperty "$key_ib_cdi_cdibaseurl" "$val_ib_cdi_cdibaseurl" 'WEB-INF/classes/config/service.properties'

rm ib-cdi.war;
jar -cvf ib-cdi.war *  > /dev/null 2>&1
rm -rf $tomcat_dir/webapps/ib-cdi*;
mv ib-cdi.war $tomcat_dir/webapps/
echo "ib-cdi.war deployed."

#---------------Deploy ib-independent-----------------------
echo "ib-independent.war deployment starting"
cd $deployables_home
rm -rf temp;
mkdir temp && cd temp;

echo "Downloading the ib-independent.war."
wget https://s3.amazonaws.com/isha-bucket/${env}/artifactory/latest/service/ib-independent.war > /dev/null 2>&1

echo "Unjar the artifacts."
jar -xvf ib-independent.war > /dev/null 2>&1

#-----------------
# ./WEB-INF/classes/framework/config/service.properties
updateProperty "$key_ib_independent_db_host" "$val_ib_independent_db_host" 'WEB-INF/classes/framework/config/service.properties'
updateProperty "$key_ib_independent_db_port" "$val_ib_independent_db_port" 'WEB-INF/classes/framework/config/service.properties'
updateProperty "$key_ib_independent_db_name" "$val_ib_independent_db_name" 'WEB-INF/classes/framework/config/service.properties'
updateProperty "$key_ib_independent_db_user" "$val_ib_independent_db_user" 'WEB-INF/classes/framework/config/service.properties'
updateProperty "$key_ib_independent_db_password" "$val_ib_independent_db_password" 'WEB-INF/classes/framework/config/service.properties'
updateProperty "$key_ib_independent_redis_host" "$val_ib_independent_redis_host" 'WEB-INF/classes/framework/config/service.properties'
updateProperty "$key_ib_independent_redis_port" "$val_ib_independent_redis_port" 'WEB-INF/classes/framework/config/service.properties'
updateProperty "$key_ib_independent_ibcdibaseurl" "$val_ib_independent_ibcdibaseurl" 'WEB-INF/classes/framework/config/service.properties'
updateProperty "$key_ib_independent_cdibaseurl" "$val_ib_independent_cdibaseurl" 'WEB-INF/classes/framework/config/service.properties'
# WEB-INF/classes/config/service.properties
updateProperty "$key_ib_independent_authurl" "$val_ib_independent_authurl" 'WEB-INF/classes/config/service.properties'

rm ib-independent.war;
jar -cvf ib-independent.war *  > /dev/null 2>&1
rm -rf $tomcat_dir/webapps/ib-independent*;
mv ib-independent.war $tomcat_dir/webapps/
echo "ib-independent.war deployed."

echo "Reset Tomcat directory owner and access permissions."
chown -R tomcat:root $tomcat_dir/webapps/ && chmod -R 755 $tomcat_dir/webapps



#-------------Setting Tomcat ENV----------------
cd $deployables_home;
mkdir tomcat;
cd tomcat;
wget 'https://s3.amazonaws.com/isha-bucket/${env}/artifactory/latest/service/tomcat/setenv.sh' > /dev/null 2>&1
cp setenv.sh $tomcat_dir/bin/
cd $deployables_home;


#-------------Create other folders---------------
# auth-keys
mkdir -p /appdata/auth/keys
wget 'https://s3.amazonaws.com/isha-bucket/${env}/artifactory/latest/service/auth-keys/key_rsa.pvt' > /dev/null 2>&1
mv key_rsa.pvt /appdata/auth/keys/
chmod -R g+rx /appdata/auth
chgrp -R tomcat /appdata/auth

# batch-uploads
mkdir -p /appdata/mdm/batch-uploads/
chmod -R g+rwx /appdata/mdm
chgrp -R tomcat /appdata/mdm

echo "Starting the Tomcat server"
service tomcat restart

exit 0

