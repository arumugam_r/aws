#--------------------------------------------------------------
# This module creates all resources necessary for deploy Layer
#--------------------------------------------------------------

variable "name"              { default = "deploy" }
variable "vpc_id"            { }
variable "vpc_cidr"          { }
variable "key_name"          { }
variable "subnet_ids"        { }
variable "ami"               { }
variable "instance_type"     { }
variable "azs"               { }
variable "env"               { }


resource "aws_security_group" "deploy" {
  name        = "${var.name}-security_group"
  vpc_id      = "${var.vpc_id}"
  description = "Deploy security group"

  tags      {
    Name = "${var.name}-security_group"
  }
  lifecycle { create_before_destroy = true }

  ingress {
    protocol    = "tcp"
    from_port   = 8080
    to_port     = 8080
    cidr_blocks = ["${var.vpc_cidr}"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["${var.vpc_cidr}"]
  }
  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Render a part using a `template_file`
data "template_file" "init" {
  template = "${file("${path.module}/deploy.sh.tpl")}"

  vars {
    host_name = "deploy"
  }
}

resource "aws_instance" "deploy" {
  ami           = "${element(split(",", var.ami), count.index)}"
  instance_type = "${var.instance_type}"
  key_name      = "${var.key_name}"
  subnet_id     = "${element(split(",", var.subnet_ids), count.index)}"
  user_data     = "${element(data.template_file.init.*.rendered, count.index)}"
  vpc_security_group_ids = ["${aws_security_group.deploy.id}"]
  tags      { Name = "${var.name}" }
  lifecycle { create_before_destroy = true }
}

output "public_ips"   { value = "${join(",", aws_instance.deploy.*.public_ip)}" }
output "private_ips"  { value = "${join(",", aws_instance.deploy.*.private_ip)}" }

