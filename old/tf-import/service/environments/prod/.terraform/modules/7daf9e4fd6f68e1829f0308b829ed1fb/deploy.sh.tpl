#!/bin/bash -ex

echo get the instance-id from meta-data.
IP=`wget http://169.254.169.254/latest/meta-data/local-ipv4  -qO - | cut -d . -f 1,2,3,4`

echo name variable for the hostname $IP
HOSTNAME=${host_name}

echo update the hostname file with the new hostname/fqdn  $HOSTNAME.ishacloud.org
echo $HOSTNAME.ishacloud.org > /etc/hostname

echo update the running hostname
hostname $HOSTNAME.ishacloud.org

echo Crate hosts file with Hostname and IP
echo "127.0.0.1 localhost" > /etc/hosts
echo $IP  $HOSTNAME.ishacloud.org >> /etc/hosts


