variable "name" {}
variable "vpc_id"             { }
variable "rds_db_name" {}
variable "rds_instance_type" {}
variable "rds_username" {}
variable "rds_password" {}
variable "rds_subnet_group_name" {}
variable "rds_access_cidr" {}



module "rds" {
  source = "./rds"

  name                 = "${var.name}"
  vpc_id              = "${var.vpc_id}"
  rds_db_name          = "${var.rds_db_name}"
  instance_class       = "${var.rds_instance_type}"
  username             = "${var.rds_username}"
  password             = "${var.rds_password}"
  db_subnet_group_name = "${var.rds_subnet_group_name}"
  rds_access_cidr      = "${var.rds_access_cidr}"
}

output "rds_url" { value = "${module.rds.rds_url}" }




