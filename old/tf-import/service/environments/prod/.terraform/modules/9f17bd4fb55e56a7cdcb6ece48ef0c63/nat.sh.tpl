#!/bin/bash
set -e
echo get the instance-id from meta-data.
IP=`wget http://169.254.169.254/latest/meta-data/local-ipv4  -qO - | cut -d . -f 1,2,3,4`

echo name variable for the hostname $IP
HOSTNAME="${host_name}"

echo update the hostname file with the new hostname/fqdn  $HOSTNAME.ishacloud.org
echo $HOSTNAME.ishacloud.org > /etc/hostname

echo update the running hostname
hostname $HOSTNAME.ishacloud.org

echo Removing existing hosts file
rm -rf /etc/hosts

echo Crate hosts file with Hostname and IP
echo $IP  $HOSTNAME.ishacloud.org > /etc/hosts