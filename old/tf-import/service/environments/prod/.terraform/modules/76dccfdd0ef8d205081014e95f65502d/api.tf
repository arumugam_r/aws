#--------------------------------------------------------------
# This module creates all resources necessary for api Layer
#--------------------------------------------------------------

variable "name"              { default = "api" }
variable "vpc_id"            { }
variable "vpc_cidr"          { }
variable "key_name"          { }
variable "subnet_ids"        { }
variable "ami"               { }
variable "instance_type"     { }
variable "azs"               { }
variable "env"               { }
variable "service_ips"       { }
variable "monitor_host_ip" { }


resource "aws_security_group" "api" {
  name        = "${var.name}-security_group"
  vpc_id      = "${var.vpc_id}"
  description = "api security group"

  tags      { 
    Name = "${var.name}-security_group"
  }
  lifecycle { create_before_destroy = true }

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    protocol    = "tcp"
    from_port   = 5666
    to_port     = 5666
    cidr_blocks = ["${var.monitor_host_ip}/31"]
  }
  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["${var.vpc_cidr}"]
  }
  ingress {
    protocol    = "icmp"
    from_port   = 8
    to_port     = -1
    cidr_blocks = ["${var.monitor_host_ip}/31"]
  }
  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Render a part using a `template_file`
data "template_file" "init" {
  template = "${file("${path.module}/api.sh.tpl")}"

  vars {
      env = "${var.env}"
      host_name = "api${count.index}"
    monitor_host_ip = "${var.monitor_host_ip}"
    service_ips = "${var.service_ips}"
  }
}

resource "aws_instance" "api" {
  ami           = "${element(split(",", var.ami), count.index)}"
  instance_type = "${var.instance_type}"
  key_name      = "${var.key_name}"
  subnet_id     = "${element(split(",", var.subnet_ids), count.index)}"
  user_data     = "${element(data.template_file.init.*.rendered, count.index)}"
  vpc_security_group_ids = ["${aws_security_group.api.id}"]

  tags      { Name = "${var.name}" }
  lifecycle { create_before_destroy = true }
}

output "instance_id"   { value = "${aws_instance.api.id}" }
output "public_ips"   { value = "${join(",", aws_instance.api.*.public_ip)}" }
output "private_ips"  { value = "${join(",", aws_instance.api.*.private_ip)}" }
