#--------------------------------------------------------------
# This module creates all resources necessary for EC2 based Nat instance
#--------------------------------------------------------------
variable "name"              { default = "nat" }
variable "vpc_id"            { }
variable "vpc_cidr"          { }
variable "key_name"          { }
variable "subnet_ids"        { }
variable "ami"               { }
variable "instance_type"     { }

resource "aws_security_group" "nat" {
  name        = "${var.name}-security_group"
  vpc_id      = "${var.vpc_id}"
  description = "${var.name} EC2 based NAT service"

  tags      {
    Name        = "${var.name}-security_group"
  }
  lifecycle { create_before_destroy = true }

  ingress {
    protocol    = "tcp"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["${var.vpc_cidr}"]
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Render a part using a `template_file`
data "template_file" "init" {
  template = "${file("${path.module}/nat.sh.tpl")}"

  vars {
    host_name = "monitor"
  }
}

resource "aws_instance" "nat" {
  ami           = "${var.ami}"
  instance_type = "${var.instance_type}"
  key_name      = "${var.key_name}"
  subnet_id     = "${element(split(",", var.subnet_ids), count.index)}"
  user_data     = "${element(data.template_file.init.*.rendered, count.index)}"
  source_dest_check = false
  vpc_security_group_ids = ["${aws_security_group.nat.id}"]
  associate_public_ip_address = true
  tags      { Name = "${var.name}" }
  lifecycle { create_before_destroy = true }
}


output "nat_id" { value = "${aws_instance.nat.id}"}
output "monitor_host_ip"  { value = "${join(",", aws_instance.nat.*.private_ip)}" }

