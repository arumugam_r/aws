variable "name" {}
variable "rds_db_name" {}
variable "username" {}
variable "password" {}
variable "instance_class" {}
variable "db_subnet_group_name" {}
variable "vpc_id"             { }
variable "rds_access_cidr" {}


resource "aws_db_parameter_group" "parameter_group" {
  name = "${var.name}-rds-parameter-group"
  description = "${var.name} RDS  Group"
  family = "${var.family_name}"
}


resource "aws_security_group" "rds_security_group" {
  name = "${var.name}-rds-security-group"
  description = "Allow all inbound traffic"
  vpc_id      = "${var.vpc_id}"


  ingress {
    from_port = 5432
    to_port = 5432
    protocol = "TCP"
    cidr_blocks = ["${split(",", var.rds_access_cidr)}"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.name}-rds-security-group"
  }
}


resource "aws_db_instance" "service-db" {
  identifier           = "${var.name}"

  allocated_storage    = "${var.allocated_storage}"
  engine               = "${var.engine}"
  engine_version       = "${var.engine_version}"
  instance_class       = "${var.instance_class}"
  name                 = "${var.rds_db_name}"
  username             = "${var.username}"
  password             = "${var.password}"
  db_subnet_group_name = "${var.db_subnet_group_name}"
  vpc_security_group_ids = ["${aws_security_group.rds_security_group.id}"]
  parameter_group_name = "${aws_db_parameter_group.parameter_group.name}"
  storage_type         = "gp2"
  multi_az             = true
  storage_encrypted    = true
  apply_immediately    = true
  allocated_storage    = 20
}

output "rds_url" { value = "${aws_db_instance.service-db.address}" }

