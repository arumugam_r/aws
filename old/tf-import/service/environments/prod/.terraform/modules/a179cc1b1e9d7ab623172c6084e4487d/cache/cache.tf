#--------------------------------------------------------------
# This module creates all resources necessary for cache Layer
#--------------------------------------------------------------

variable "name"              { default = "cache" }
variable "vpc_id"            { }
variable "vpc_cidr"          { }
variable "key_name"          { }
variable "subnet_ids"        { }
variable "ami"               { }
variable "instance_type"     { }
variable "azs"               { }
variable "env"               { }
variable "rds_url"           { }
variable "rds_super_user"           { }
variable "rds_password"           { }
variable "service_subnet_cidrs"     { }
variable "monitor_host_ip" 		{ }

resource "aws_security_group" "cache" {
  name        = "${var.name}-security_group"
  vpc_id      = "${var.vpc_id}"
  description = "cache security group"

  tags      {
    Name = "${var.name}-security_group"
  }
  lifecycle { create_before_destroy = true }

  ingress {
    protocol    = "tcp"
    from_port   = 2181
    to_port     = 2181
    cidr_blocks = ["${var.service_subnet_cidrs}"]
  }
  ingress {
    protocol    = "tcp"
    from_port   = 6379
    to_port     = 6379
    cidr_blocks = ["${var.service_subnet_cidrs}"]
  }
  ingress {
    protocol    = "tcp"
    from_port   = 8983
    to_port     = 8983
    cidr_blocks = ["${var.service_subnet_cidrs}"]
  }
  ingress {
    protocol    = "tcp"
    from_port   = 5666
    to_port     = 5666
    cidr_blocks = ["${var.monitor_host_ip}/31"]
  }
  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["${var.vpc_cidr}"]
  }
  ingress {
    protocol    = "icmp"
    from_port   = 8
    to_port     = -1
    cidr_blocks = ["${var.monitor_host_ip}/31"]
  }
  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Render a part using a `template_file`
data "template_file" "init" {
  template = "${file("${path.module}/cache.sh.tpl")}"

  vars {
    env = "${var.env}"
    host_name = "cache${count.index}"
    monitor_host_ip = "${var.monitor_host_ip}"
      rds_url = "${var.rds_url}"
    rds_super_user = "${var.rds_super_user}"
    rds_password = "${var.rds_password}"
  }
}

resource "aws_instance" "cache" {
  ami           = "${element(split(",", var.ami), count.index)}"
  instance_type = "${var.instance_type}"
  key_name      = "${var.key_name}"
  subnet_id     = "${element(split(",", var.subnet_ids), count.index)}"
  user_data     = "${element(data.template_file.init.*.rendered, count.index)}"
  vpc_security_group_ids = ["${aws_security_group.cache.id}"]
  tags      { Name = "${var.name}" }
  lifecycle { create_before_destroy = true }
}

output "public_ips"   { value = "${join(",", aws_instance.cache.*.public_ip)}" }
output "private_ips"  { value = "${join(",", aws_instance.cache.*.private_ip)}" }
