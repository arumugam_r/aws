#--------------------------------------------------------------
# This module creates all compute resources
#--------------------------------------------------------------

variable "name"                       { }
variable "region"                     { }
variable "vpc_id"                     { }
variable "vpc_cidr"                   { }
variable "env"                        { }
variable "key_name"                   { }
variable "api_subnet_ids"             { }
variable "api_subnet_cidrs"           { }
variable "service_subnet_ids"         { }
variable "service_subnet_cidrs"       { }
variable "cache_subnet_ids"           { }
variable "azs"                        { }
variable "rds_url"                    { }
variable "api_ami"                    { }
variable "api_instance_type"          { }
variable "service_ami"                { }
variable "service_instance_type"      { }
variable "cache_ami"                  { }
variable "cache_instance_type"        { }
variable "deploy_ami"                  { }
variable "deploy_instance_type"        { }
variable "rds_super_user"          		 { }
variable "rds_password"       	  	  { }
variable "monitor_host_ip"				 { }

resource "aws_eip" "api_eip" {
  instance = "${module.api.instance_id}"
  vpc      = true
}

module "cache" {
  source = "./cache"
  name                = "${var.name}-cache"
  vpc_id              = "${var.vpc_id}"
  vpc_cidr            = "${var.vpc_cidr}"
  key_name            = "${var.key_name}"
  subnet_ids          = "${var.cache_subnet_ids}"
  ami                 = "${var.cache_ami}"
  azs                 = "${var.azs}"
  env                 = "${var.env}"
  instance_type       = "${var.cache_instance_type}"
  rds_url             = "${var.rds_url}"
  rds_super_user      = "${var.rds_super_user}"
  rds_password        = "${var.rds_password}"
  service_subnet_cidrs = "${var.service_subnet_cidrs}"
    monitor_host_ip   	= "${var.monitor_host_ip}"
  
}



module "service" {
  source = "./service"
  name                = "${var.name}-service"
  vpc_id              = "${var.vpc_id}"
  vpc_cidr            = "${var.vpc_cidr}"
  key_name            = "${var.key_name}"
  subnet_ids          = "${var.service_subnet_ids}"
  ami                 = "${var.service_ami}"
  azs                 = "${var.azs}"
  env                 = "${var.env}"
  instance_type       = "${var.service_instance_type}"
  rds_url             = "${var.rds_url}"
  rds_super_user      = "${var.rds_super_user}"
  rds_password        = "${var.rds_password}"
  redis_ips           = "${module.cache.private_ips}"
  solr_ips            = "${module.cache.private_ips}"
  api_subnet_cidrs   	= "${var.api_subnet_cidrs}"
  monitor_host_ip   	= "${var.monitor_host_ip}"
}


module "deploy" {
  source = "./deploy"
  name                = "${var.name}-deploy"
  vpc_id              = "${var.vpc_id}"
  vpc_cidr            = "${var.vpc_cidr}"
  key_name            = "${var.key_name}"
  subnet_ids          = "${var.service_subnet_ids}"
  ami                 = "${var.deploy_ami}"
  azs                 = "${var.azs}"
  env                 = "${var.env}"
  instance_type       = "${var.deploy_instance_type}"
}

module "api" {
  source = "./api"
  name                = "${var.name}-api"
  vpc_id              = "${var.vpc_id}"
  vpc_cidr            = "${var.vpc_cidr}"
  key_name            = "${var.key_name}"
  subnet_ids          = "${var.api_subnet_ids}"
  ami                 = "${var.api_ami}"
  azs                 = "${var.azs}"
  env                 = "${var.env}"
  instance_type       = "${var.api_instance_type}"
  service_ips         = "${module.service.private_ips}"
    monitor_host_ip   	= "${var.monitor_host_ip}"
  
}

#output "service_private_ips"  { value = "${module.service.private_ips}" }
#output "service_public_ips"   { value = "${module.service.public_ips}" }
