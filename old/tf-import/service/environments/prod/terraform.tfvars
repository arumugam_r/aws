#--------------------------------------------------------------
# Service Layer VPC Setup
#--------------------------------------------------------------


name              = "isha-srv"
env               = "prod"
sub_domain        = "ishacloud.org"
region            = "us-east-1"
azs               = "us-east-1e,us-east-1c" # AZs are region specific

#--------------------------------------------------------------
# Network
#--------------------------------------------------------------

service_vpc_cidr        = "10.0.32.0/20"

api_subnet_cidrs       = "10.0.32.0/24"               # Creating one public subnet only on one AZ for NAT
service_subnet_cidrs    = "10.0.34.0/24"              # Creating one private subnet per AZ
cache_subnet_cidrs      = "10.0.36.0/24"              # Creating one private subnet per AZ
rds_subnet_cidrs        = "10.0.38.0/24,10.0.39.0/24" # Creating one private subnet per AZ


key_name = "Isha-Service"

#--------------------------------------------------------------
# EC2 Instances
#--------------------------------------------------------------
api_instance_type       = "t2.micro"
api_ami                 = "ami-2f97d738"

service_instance_type   = "t2.small"
service_ami             = "ami-7c95d56b"

deploy_instance_type   	= "t2.small"
deploy_ami             	= "ami-04b7f713"


cache_instance_type     = "t2.small"
cache_ami               = "ami-99b1ca8e"

#--------------------------------------------------------------
# NAT Instance instead of NAT gateway.
#--------------------------------------------------------------

nat_instance_type       = "t2.micro"
nat_ami                 = "ami-0d94d41a"

#--------------------------------------------------------------
# EC2 Instances
#--------------------------------------------------------------
rds_db_name             = "ISHAServiceDB"
rds_instance_type       = "db.m3.medium"
rds_username            = "ishasrvprod"
rds_password            = "isha_srv_it_230"
parameter_group_name    = "rds_prod_parameter_group"
