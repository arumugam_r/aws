#!/bin/bash

DB_NAME=$1;
export PGPASSWORD="mdmServer";
tns=`echo "SELECT tablename FROM pg_tables WHERE schemaname = 'public'"|psql -U mdm_server $DB_NAME -h <hostname> -t`
tables=($tns);


for tn in ${tables[*]}
do
	echo "Truncating table: " $tn
	echo "truncate $tn cascade"|psql -U mdm_server -h <hostname> $DB_NAME
done
